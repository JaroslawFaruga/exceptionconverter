using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionConverter.TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                new ThrowException().Run();
            }
            catch (Exception ex)
            {
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                for (int i = 0; i < 100000; ++i)
                {
                    new NetExceptionToString().Convert(ex);
                }

                stopwatch.Stop();
                Console.WriteLine($"{stopwatch.Elapsed.ToString()}");

                Stopwatch stopwatch2 = new Stopwatch();
                stopwatch2.Start();

                for (int i = 0; i < 100000; ++i)
                {
                    new ConvertExceptionToString().Convert(ex);
                }

                stopwatch2.Stop();
                Console.WriteLine($"{stopwatch2.Elapsed.ToString()}");
                               
                Console.WriteLine(ex.ToString());
                
                Console.WriteLine("ConvertExceptionToString");
                Console.Write(new ConvertExceptionToString().Convert(ex));
            }

            Console.WriteLine("Completed!");
            Console.ReadKey();
        }
    }
}
