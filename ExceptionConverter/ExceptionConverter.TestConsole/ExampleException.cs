﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionConverter.TestConsole
{
    public class ExampleException : Exception
    {
        public ExampleException()
        {
        }

        public ExampleException(string message) : base(message)
        {
        }

        public ExampleException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExampleException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
