﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionConverter.TestConsole
{
    public class ThrowException
    {
        public void Run()
        {
            int x = 5;
            int y = 0;

            try
            {
                var z = x / y;
            }
            catch (Exception ex)
            {
                throw new ExampleException("Mesage of ExampleException", ex);
            }

        }
    }
}
