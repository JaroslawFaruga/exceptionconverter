﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionConverter
{
    public class NetExceptionToString : IExceptionToString
    {
        public string Convert(Exception ex)
        {
            return ex.ToString();
        }
    }
}
