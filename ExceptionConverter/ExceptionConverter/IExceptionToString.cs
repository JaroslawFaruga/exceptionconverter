﻿using System;

namespace ExceptionConverter
{
    public interface IExceptionToString
    {
        string Convert(Exception ex);
    }
}
