﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionConverter
{
    public class ConvertExceptionToString : IExceptionToString
    {
        public string Convert(Exception ex)
        {
            var sb = new StringBuilder();

            GetMessagesWithTypes(sb, ex);
            GetDetails(sb, ex);
            GetStackTrace(sb, ex);
            
            return sb.ToString();
        }

        private void GetMessagesWithTypes(StringBuilder sb, Exception ex)
        {
            while (true)
            {
                sb.Append(ex.GetType().FullName);
                sb.Append(": ");
                sb.Append(ex.Message);
                ex = ex.InnerException;
                if (ex != null)
                {
                    sb.Append(" ---> ");
                }
                else
                {
                    break;
                }
            }

            sb.Append(Environment.NewLine);
        }

        private void GetDetails(StringBuilder sb, Exception ex)
        {
            sb.Append("HResult: ");
            sb.Append(ex.HResult);
            sb.Append(Environment.NewLine);

            sb.Append("Source: ");
            sb.Append(ex.Source);
            sb.Append(Environment.NewLine);

            sb.Append("TargetSite: ");
            sb.Append(ex.TargetSite);
            sb.Append(Environment.NewLine);
        }

        private void GetStackTrace(StringBuilder sb, Exception ex)
        {
            var exceptions = GetExceptions(ex).Reverse();
            
            foreach (var e in exceptions)
            {
                sb.Append("   --- ");
                sb.Append(e.GetType().Name);
                sb.Append(Environment.NewLine);
                GetSingleStackTrace(sb, e);
            }
        }

        private IEnumerable<Exception> GetExceptions(Exception ex)
        {
            while (ex != null)
            {
                yield return ex;
                ex = ex.InnerException;
            }
        }

        private void GetSingleStackTrace(StringBuilder sb, Exception ex)
        {
            var stackTrace = new StackTrace(ex, true);

            foreach (var frame in stackTrace.GetFrames())
            {
                var fileName = frame.GetFileName();

                var method = frame.GetMethod();
                sb.Append("   in ");
                sb.Append(method.ReflectedType.Namespace);
                sb.Append(".");
                sb.Append(method.ReflectedType.Name);
                sb.Append(" ");

                sb.Append(method.ToString());
                sb.Append(" IL: ");
                sb.Append(frame.GetILOffset());

                
                if (fileName != null)
                {
                    sb.Append(" in ");
                    sb.Append(fileName);
                    sb.Append(" ");
                    sb.Append(frame.GetFileLineNumber());
                }

                sb.Append(Environment.NewLine);
            }
        }
    }
}
